package info.androidhive.gpluslogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;

public class DangTinActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView txtTenChuTro,txtPlace,txtGia,txtthang,txtSoPhong;
    private EditText edtSoPhong,edtGia,edtPlace,edtChuTro;
    private ImageView imgDangTin;
    private Socket msocket;{
        try{
            msocket= IO.socket("http://192.168.100.104:3000");

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dang_tin);
        msocket.connect();
        FindID();

        imgDangTin.setOnClickListener(this);
    }

    private void FindID(){
        txtTenChuTro= (TextView) findViewById(R.id.txtTenChuTro);
        txtPlace= (TextView) findViewById(R.id.txtPlace);
        txtGia= (TextView) findViewById(R.id.txtGia);
        txtthang= (TextView) findViewById(R.id.txtthang);
        txtSoPhong= (TextView) findViewById(R.id.txtSoPhong);
        edtSoPhong= (EditText) findViewById(R.id.edtSoPhong);
        edtGia= (EditText) findViewById(R.id.edtGia);
        edtPlace= (EditText) findViewById(R.id.edtPlace);
        edtChuTro= (EditText) findViewById(R.id.edtChuTro);
        imgDangTin= (ImageView) findViewById(R.id.imgDangTin);
        imgDangTin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgDangTin:
                Intent intent= new Intent(DangTinActivity.this, MapActivity.class);
                intent.putExtra("TenCT",edtChuTro.getText().toString());
                intent.putExtra("DiaDiem",edtPlace.getText().toString());
                intent.putExtra("Gia",edtGia.getText().toString());
                intent.putExtra("SoPhong", edtSoPhong.getText().toString());
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode==1){
            //đây là giá trị được trả về từ SingupActivi
            //lấy giá trị cũ được trả về từ Singupactivity
            edtChuTro.setText(data.getStringExtra("TenCT"));
            edtPlace.setText(data.getStringExtra("DiaDiem"));
            edtGia.setText(data.getStringExtra("Gia"));
            edtSoPhong.setText(data.getStringExtra("SoPhong"));
        }
    }
}
