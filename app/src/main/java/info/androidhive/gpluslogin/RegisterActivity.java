package info.androidhive.gpluslogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView txtName,txtEmail,txtPass;
    private EditText edtName,edtEmail,edtPass;
    private Button btnRegister;
    private Socket msocket;{
        try{
            msocket= IO.socket("http://192.168.100.104:3000");

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        findID();
        msocket.connect();
        msocket.on("ketquaDangKyUn",onNewClient);
        msocket.on("server-gui-username",OnGuiUsername);
        btnRegister.setOnClickListener(this);
    }

    private void findID(){
        txtName= (TextView) findViewById(R.id.txtName);
        txtEmail= (TextView) findViewById(R.id.txtEmail);
        txtPass= (TextView) findViewById(R.id.txtPass);
        edtName= (EditText) findViewById(R.id.edtName);
        edtEmail= (EditText) findViewById(R.id.edtEmail);
        edtPass= (EditText) findViewById(R.id.edtPass);
        btnRegister= (Button) findViewById(R.id.btnRegister);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnRegister:
                msocket.emit("client-gui-username",edtName.getText().toString());
                Intent intent3 = new Intent(RegisterActivity.this, MainActivity.class);
                startActivity(intent3);
                break;
        }

    }
    private Emitter.Listener onNewClient= new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data= (JSONObject) args[0];
                    String noidung;

                    try {
                        noidung= data.getString("noidung");
                        if (noidung=="true"){
                            Toast.makeText(RegisterActivity.this, "Dang ky thanh cong", Toast.LENGTH_SHORT).show();
                        }else {
                            Toast.makeText(RegisterActivity.this, "That bai", Toast.LENGTH_SHORT).show();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
    private Emitter.Listener OnGuiUsername= new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data= (JSONObject) args[0];
                    JSONArray noidung;
                    try {
                        noidung= data.getJSONArray("danhsach");
                        Toast.makeText(RegisterActivity.this, noidung.length()+"", Toast.LENGTH_SHORT).show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        }
    };
}
