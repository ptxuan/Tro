package info.androidhive.gpluslogin;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import java.net.URISyntaxException;
import java.util.ArrayList;

public class MapActivity extends AppCompatActivity implements View.OnClickListener{

    private TextView txtHienThi;
    private ImageView imgthemtin;
    private ArrayList<String> lstDangtin;
    private Socket msocket;{
        try{
            msocket= IO.socket("http://192.168.100.104:3000");

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        msocket.connect();
        findID();

        String TenCT="";
        String DiaDiem="";
        String Gia="";
        String SoPhong="";
        if(getIntent()!=null){
            //Lấy dữ liệu dựa vào key đã truyền từ Activity trước
            TenCT=getIntent().getStringExtra("TenCT");
            DiaDiem= getIntent().getStringExtra("DiaDiem");
            Gia=getIntent().getStringExtra("Gia");
            SoPhong= getIntent().getStringExtra("SoPhong");
        }
        txtHienThi.setText("Tên chủ trọ"+ TenCT + "\n"+"Địa điểm: "+DiaDiem+"\n"+"Giá"+Gia+"\n"+"Số phòng"+SoPhong);
        imgthemtin.setOnClickListener(this);


    }
    private void findID(){
        txtHienThi= (TextView) findViewById(R.id.txtTenChuTro);
        imgthemtin= (ImageView) findViewById(R.id.imgthemtin);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.imgthemtin:
                Intent intent = new Intent();
                setResult(1,intent);
                break;
        }
    }
}
