package info.androidhive.gpluslogin;

/**
 * Created by MyPC on 7/25/2018.
 */

public class NhaTro {
    private String tenChuTro;
    private String diaDiem;
    private int Gia;
    private int soPhongtrong;

    public NhaTro() {
    }

    public NhaTro(String tenChuTro, String diaDiem, int gia, int soPhongtrong) {
        this.tenChuTro = tenChuTro;
        this.diaDiem = diaDiem;
        Gia = gia;
        this.soPhongtrong = soPhongtrong;
    }

    public String getTenChuTro() {
        return tenChuTro;
    }

    public void setTenChuTro(String tenChuTro) {
        this.tenChuTro = tenChuTro;
    }

    public String getDiaDiem() {
        return diaDiem;
    }

    public void setDiaDiem(String diaDiem) {
        this.diaDiem = diaDiem;
    }

    public int getGia() {
        return Gia;
    }

    public void setGia(int gia) {
        Gia = gia;
    }

    public int getSoPhongtrong() {
        return soPhongtrong;
    }

    public void setSoPhongtrong(int soPhongtrong) {
        this.soPhongtrong = soPhongtrong;
    }
}
