package info.androidhive.gpluslogin;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by MyPC on 7/27/2018.
 */

public class NhaTroAdapter extends BaseAdapter {

    private Context context;
    private int layout;
    private ArrayList<NhaTro> lstNhaTro;

    public NhaTroAdapter(Context context, int layout, ArrayList<NhaTro> lstNhaTro) {
        this.context = context;
        this.layout = layout;
        this.lstNhaTro = lstNhaTro;
    }

    private class ViewHolder{
        TextView txtTenChu;
        TextView txtDiaDiem;
        TextView txtGiaphong;
        TextView txtSoPhongtrong;
    }

    @Override
    public int getCount() {
        return lstNhaTro.size();
    }

    @Override
    public NhaTro getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
